# Demo with api version 20190325

[app_image_search_general_v20190510_1.ipynb](https://github.com/jianhuashao/AlibabaCloud_ImageSearch_Demo_py2/blob/master/app_image_search_general_v20190510_1.ipynb)

Pls set environment for key and ID by:
```sh
export accessKeyId="xx"
export accessKeySecret="yy"
```

# This works only with previous version of API and is now deprecated.

[py2 notebook demo](https://github.com/jianhuashao/AlibabaCloud_ImageSearch_Demo_py2/blob/master/app_image_search_py2.ipynb)
